const express = require('express')
const bodyParser = require('body-parser')
const queries = require('./queries')
const app = express()

app.listen(3000)

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', (req, res) => {
    res.send('Users app')
})

app.get('/users', queries.allUsers)
app.get('/max-following', queries.maxFollowing)
app.get('/not-following', queries.notFollowing)
app.get('/users/:id/friends', queries.userInfo)
