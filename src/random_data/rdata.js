const fs = require('fs')
const names = fs.readFileSync('src/random_data/names.txt', 'utf-8').split('\n')


const rdata = {
    rindex: (arr_length) => {
        return Math.floor(Math.random() * arr_length)
    },
    gender: () => {
        return ['male', 'female'][rdata.rindex(2)]
    },
    names: () => {
        return names
    }
}


module.exports = rdata
