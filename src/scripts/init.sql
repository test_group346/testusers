DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
    ID SERIAL PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    gender VARCHAR(10) NOT NULL
);
DROP TABLE IF EXISTS subscriptions;
CREATE TABLE subscriptions (
    ID SERIAL PRIMARY KEY,
    user_id int NOT NULL,
    subs_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE(user_id, subs_id)
);

DROP FUNCTION IF EXISTS enforce_subscriptions;
CREATE FUNCTION enforce_subscriptions() RETURNS trigger AS
$$
DECLARE
    max_sub_count INTEGER := 150;
    must_check BOOLEAN := FALSE;
    sub_count INTEGER := 0;
BEGIN
    IF TG_OP = 'INSERT' THEN
        must_check := TRUE;
    END IF;

    IF must_check THEN
        LOCK TABLE subscriptions IN EXCLUSIVE MODE;

        SELECT INTO sub_count COUNT(*)
        FROM subscriptions
        WHERE user_id = NEW.user_id;

        IF sub_count >= max_sub_count THEN
            RAISE EXCEPTION 'Cannot insert more than % subscriptions for each user', max_sub_count;
         END IF;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql ;

CREATE TRIGGER enforce_subscriptions
    BEFORE INSERT ON subscriptions
    FOR EACH ROW EXECUTE PROCEDURE enforce_subscriptions();

DROP FUNCTION IF EXISTS getAllUserData;
CREATE FUNCTION getAllUserData()
	RETURNS TABLE(name_ varchar, gender varchar, count_ bigint)
	LANGUAGE sql AS
$$
	SELECT users.first_name, users.gender, (
		SELECT COUNT(*) FROM subscriptions
		WHERE users.id = subscriptions.user_id
	) FROM users ORDER BY users.first_name;
$$;

DROP FUNCTION IF EXISTS getFirstFive;
CREATE FUNCTION getFirstFive()
	RETURNS TABLE(name_ varchar, gender varchar, count_ bigint)
	LANGUAGE sql AS
$$
	SELECT name_, gender, count_ FROM getAllUserData()
    	ORDER BY count_ DESC LIMIT 5;
$$;

DROP FUNCTION IF EXISTS getOnlyZero;
CREATE FUNCTION getOnlyZero()
    RETURNS TABLE(name_ varchar, gender varchar, count_ bigint)
    LANGUAGE sql AS
$$
    SELECT name_, gender, count_ FROM getAllUserData()
        WHERE count_ = 0;
$$;

DROP FUNCTION IF EXISTS getUserInfo;
CREATE FUNCTION getUserInfo(id_ int, order_by int)
	RETURNS TABLE(name_ varchar, gender varchar, friends varchar[])
	LANGUAGE sql AS
$$
	WITH friends_ids AS (
		SELECT subscriptions.subs_id FROM subscriptions
		INNER JOIN users ON subscriptions.user_id = users.id
		WHERE users.id = id_
	)
	SELECT users.first_name, users.gender, ARRAY(
		SELECT users.first_name FROM users
		INNER JOIN friends_ids as friend ON friend.subs_id = users.id
		ORDER BY order_by ASC
	) FROM users
	WHERE users.id = id_
$$;