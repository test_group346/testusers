const rdata = require('../random_data/rdata')
const queries = require('../queries')


const randomizeSubs = (rows, rand_count=1) => {

    if (rand_count === 5) {
        return
    }

    let records = []

    for (let i = 0; i < rows.length; i++) {
        let user_id = rows[rdata.rindex(rows.length)].id,
            sub_id = rows[rdata.rindex(rows.length)].id
        records.push(`(${user_id}, ${sub_id})`)
    }

    queries.pool.query(
        "INSERT INTO subscriptions (user_id, subs_id) VALUES " + records.concat() + " RETURNING *", [], (err2, res2) => {
            if (err2) { 
                return randomizeSubs(rows, rand_count+1) 
            }
            console.log(`Added ${res2.rows.length} subscriptions`)
        }
    )
}


const insertUsers = () => {

    let records = []

    for (let name of rdata.names()) {
        records.push(`('${name}', '${rdata.gender()}')`)
    }

    console.log(records.concat())

    queries.pool.query(
        'INSERT INTO users (first_name, gender) VALUES ' + records.concat() + ' RETURNING *', [] ,(err, res) => {
            if (err) { throw err }
            console.log(`Added new ${res.rows.length} rows !`)
            randomizeSubs(res.rows)
        }
    )
}

insertUsers()
