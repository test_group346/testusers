const Pool = require('pg').Pool
const pool = new Pool({
    user: 'admin',
    host: 'dbserver',
    database: 'api',
    password: 'password',
    port: 5432,
})


const createSimpleRoute = (query) => (req, res) => {
    pool.query(query, [], (sq_err, sq_res) => {
        if (sq_err) { throw sq_err}
        res.status(200).json(sq_res.rows)
    })
}

const allUsers = createSimpleRoute('SELECT * FROM getAllUserData();')
const maxFollowing = createSimpleRoute('SELECT * FROM getFirstFive();')
const notFollowing = createSimpleRoute('SELECT * FROM getOnlyZero();')
const userInfo = (req, res, next) => {
    let {order_by, order_type} = req.query
    const id = parseInt(req.params.id),
          fields = {'id': 1, 'first_name': 2, 'gender': 3}

    if ( !order_by || !order_type) {
        res.status(400).json({
            error: 'No `order_type` or `order_by` params'
        })
        return
    }

    if ( !['asc', 'desc'].includes(order_type.toLowerCase())) {
        res.status(400).json({
            error: 'Incorrect `order_type`'
        })
        return
    }

    if ( !Object.keys(fields).includes(order_by.toLowerCase())) {
        res.status(400).json({
            error: "No such field"
        })
        return
    }

    order_by = fields[order_by] || 1

    pool.query('SELECT * FROM getUserInfo($1, $2)', [id, order_by], (sq_err, sq_res) => {
        if (sq_err) {throw sq_err}

        if (order_type.toLowerCase() === 'desc') {
            sq_res.rows[0].friends.reverse()
        }
        res.status(200).json(sq_res.rows[0])
    })
}

module.exports = {
    pool,
    allUsers,
    maxFollowing,
    notFollowing,
    userInfo
}