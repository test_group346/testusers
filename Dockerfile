FROM node:latest

RUN mkdir /backend
WORKDIR /backend

COPY package.json .
RUN mkdir /src
COPY ./src ./src
RUN npm install

EXPOSE 3000

CMD ["./src/scripts/start.sh"]
